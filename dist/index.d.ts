interface IExtension {
    load(): void;
    run(): void;
}
declare class Extension implements IExtension {
    load(): void;
    run(): void;
}

declare const foo = "foo";

export { Extension, foo };
