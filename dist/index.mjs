// src/extension.ts
var Extension = class {
  load() {
    console.log("loading...");
  }
  run() {
    console.log("running...");
  }
};

// src/index.ts
var foo = "foo";
export {
  Extension,
  foo
};
